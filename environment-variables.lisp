(in-package #:environment-variables)

(defmacro define-environment-parameter (parameter-name (environment-variable default-value) docstring &body body)
  "Define PARAMETER-NAME through a defparameter form. Its value is set to
ENVIRONMENT-VARIABLE if it is set in the environment, otherwise it is set
to DEFAULT-VALUE. BODY can be used to check for valid state."
  `(progn
     (defparameter ,parameter-name ,(if (uiop:getenv environment-variable)
                                      (uiop:getenv environment-variable)
                                      default-value)
       ,docstring)
     ,@body))
