(in-package #:cl-user)

(defpackage environment-variables
  (:use cl)
  (:export define-environment-parameter))

(defpackage audio
  (:use cl external-program)
  (:export play-file
           halt-currently-playing-audio
           text-to-speech))

(defpackage grammar
  (:use esrap cl)
  (:export parse-beep-tags))

(defpackage beep-tag
  (:use cl bordeaux-threads audio environment-variables)
  (:export batch-play-beep-tag))

(defpackage irc-bot
  (:use cl grammar beep-tag environment-variables cl-irc))
