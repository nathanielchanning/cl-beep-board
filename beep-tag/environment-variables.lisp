(in-package #:beep-tag)

(define-environment-parameter *sounds-directory* ("BEEP_BOARD_SOUNDS_PATH" "/home/channing/mnt/")
    "The directory that holds the sound files. This should not be changed at runtime."
  ;; If the directory exists, ensure there is a .bookmarks file.
  (if (uiop:directory-exists-p *sounds-directory*)
      (open (merge-pathnames *sounds-directory* ".bookmarks") :direction :probe :if-does-not-exist :create)
      (error "~a is not a valid sounds directory." *sounds-directory*)))
