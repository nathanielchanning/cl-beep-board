(in-package #:beep-tag)


(defun get-random-matching-file (tag-name)
  "Gets a random sound file that starts with
TAG-NAME if one exists."
  ;; This could be faster with a generalized binary search, but
  ;; this would require uiop:directory-files to return something other than
  ;; a list.
  (let ((valid-files (loop for file in (uiop:directory-files *sounds-directory*)
                           if (and (equal 0 (search tag-name (pathname-name file)))
                                   (equal "ogg" (pathname-type file)))
                             collect file)))
    (when valid-files
      (nth (random (length valid-files)) valid-files))))

(defun get-sound-file (tag-name)
  "Get a sound file that matches TAG-NAME
exactly or a sound file that starts with TAG-NAME."
  (let ((file-path (merge-pathnames *sounds-directory* (concatenate 'string tag-name ".ogg"))))
    (if (uiop:file-exists-p file-path)
        file-path
        (get-random-matching-file tag-name))))

(defun contains-all-terms-p (list-of-terms string)
  "Searchs STRING for all terms in LIST-OF-TERMS. If
all terms are found, T is returned. Otherwise, NIL
is returned."
  (not (position nil (mapcar (lambda (term) (search term string)) list-of-terms))))

(defun grep-sound-files (grep-terms)
  "Return sound files that contains all grep terms
in GREP-TERMS."
  (loop for file in (uiop:directory-files *sounds-directory*)
        for file-name = (pathname-name file)
        if (contains-all-terms-p grep-terms file-name)
          collect file))

(defun shuffle-sequence (unshuffled-list)
  "Shuffles UNSHUFFLED-LIST into a pseudo-random order."
  (let ((shuffling-vector (make-array (length unshuffled-list)
                                      :initial-contents unshuffled-list)))
    ;; Choose a pseudo-random item to shuffle from the vector.
    ;; Swap it with the last item in the vector.
    ;; Decrement the vector's length to remove the shuffled
    ;;   item from the pool of items to shuffle.
    (loop for i from (length shuffling-vector) downto 1
          collect (let* ((random-position (random i)))
                    (symbol-macrolet ((shuffle-item (aref shuffling-vector random-position))
                                      (swap-item    (aref shuffling-vector (1- i))))
                      (prog1
                          shuffle-item
                        (rotatef shuffle-item swap-item)))))))

(defun grep-user-tags (list-of-terms)
  "Get a list of all user tags that that contains
everything in LIST-OF-TERMS."
  (with-open-file (stream (merge-pathnames *sounds-directory* ".bookmarks"))
    (loop for line = (read-line stream nil 'done)
          until (eq line 'done)
          for user-tag = (subseq line 0 (position #\SPACE line))
          when (contains-all-terms-p list-of-terms user-tag)
            collect user-tag)))

(defun find-user-tag (term)
  "Find the user tag defined by TERM or one that starts with TERM,
and return a list of arguments if it exists."
  (let ((user-tag (make-string-output-stream)))
    (external-program:run "look" (list term (merge-pathnames *sounds-directory* ".bookmarks"))
                          :output user-tag)
    ;; Get look's output and remove the newline.
    (setf user-tag (string-trim '(#\NewLine) (get-output-stream-string user-tag)))
    (unless (string= "" user-tag)
      (split-sequence:split-sequence #\SPACE user-tag))))

(defun add-user-tag (&key user-tag-name file (start nil) (end nil))
  "Save USER-TAG-NAME as a new user tag if it doesn't already exist."
  (unless (string= user-tag-name (first (find-user-tag user-tag-name)))
    (let ((line-to-save (format nil "~a ~a~@[ -k ~a~]~@[ -K ~a~]" user-tag-name file start end))
          (bookmarks-file (merge-pathnames *sounds-directory* ".bookmarks")))
      (external-program:run "sort"
                            (list "-o" bookmarks-file "-m" "-" bookmarks-file)
                            :input (make-string-input-stream line-to-save)))))
