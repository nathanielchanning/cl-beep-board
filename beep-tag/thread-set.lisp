(in-package #:beep-tag)


;;; This can be switched to an integer
;;; and a destroy flag in beep-tag.lisp
;;; if the hash table has performance
;;; or memory problems.
(defvar *thread-set* (make-hash-table)
  "A set of all the currently running threads.")

(defvar *thread-set-lock* (make-lock))

(defun add-to-thread-set (thread-key)
  "Adds THREAD-KEY to the thread set."
  (with-lock-held (*thread-set-lock*)
    (setf (gethash thread-key *thread-set*) t)))

(defun remove-from-thread-set (thread-key)
  "Removes THREAD-KEY from the thread-set."
  (with-lock-held (*thread-set-lock*)
    (remhash thread-key *thread-set*)))

(defmacro with-thread-set ((thread-key) &body body)
  `(unwind-protect
        (progn
          (add-to-thread-set ,thread-key)
          ,@body)
     (remove-from-thread-set ,thread-key)))

(defun destroy-all-threads-except (exception-thread-key)
  "Destroys all threads in the thread set except
for the thread identified by EXCEPTION-THREAD-KEY."
  (with-lock-held (*thread-set-lock*)
    ;; This destroys the threads and any future
    ;; tags that may be handled, but doesn't necessarily
    ;; stop all play back.
    (maphash (lambda (key val)
               (declare (ignore val))
               (unless (eql key exception-thread-key)
                 (destroy-thread key)
                 (remhash key *thread-set*)))
             *thread-set*)))
