(in-package #:beep-tag)

;;; Use describe 'beep-tag-type for all
;;; available beep tag commands.
(deftype beep-tag-type ()
  '(member :help :help-user-tags :text-to-speech
           :full-halt-play-back :halt-play-back
           :user-tag-name :grep-tag-names
           :grep-user-tag-names :tag-name))

;;;----------Thread-Arguments-----------
;;; These variables are used for function
;;; arguments when BATCH-PLAY-BEEP-TAG calls
;;; BATCH-PLAY-BEEP-TAG-THREAD.
(defvar *callback-function* nil
  "Holds a function that is used to transfer
any information requested by the user, such as
file names with a 'help' beep tag. This function
takes a single STRING argument.")

(defvar *type-and-args-list* nil
  "Holds a list of beep-tag types and their
corresponding arg lists.")

(defgeneric play-beep-tag (beep-tag-type args)
  (:documentation "Processes ARGS based on BEEP-TAG-TYPE.
ARGS is dependent on BEEP-TAG-TYPE."))

(defmacro define-play-beep-tag (beep-tag-type-keyword (&rest lambda-list) &body body)
  "Define a new PLAY-BEEP-TAG method specializing on BEEP-TAG-TYPE-KEYWORD."
  (if lambda-list
      `(DEFMETHOD play-beep-tag ((beep-tag-type (eql ,beep-tag-type-keyword)) args)
         (declare (ignore beep-tag-type))
         (destructuring-bind ,lambda-list
             args
           ,@body))
      `(DEFMETHOD play-beep-tag ((beep-tag-type (eql ,beep-tag-type-keyword)) args)
         (declare (ignore beep-tag-type args))
         ,@body)))

(define-play-beep-tag :help ()
  ;; Iterate over every file in the sounds directory.
  (dolist (file (uiop:directory-files *sounds-directory*))
    (let* ((name (pathname-name file))
           (first-char (aref name 0)))
      ;; Send ogg files and
      ;; don't send hidden files.
      (when (and (not (equal #\. first-char))
                 (equal "ogg" (pathname-type file)))
        (funcall *callback-function* name)))))

(define-play-beep-tag :help-user-tags ()
  ;; The .bookmarks file has the name of every
  ;; user tag stored as the first sequence on every line succeeded
  ;; by a space, so open up the .bookmarks file and send every
  ;; user tag into the callback function.
  (with-open-file (stream (merge-pathnames *sounds-directory* ".bookmarks"))
    (loop for line = (read-line stream nil 'done)
          until (eq line 'done)
          do (let ((user-tag (subseq line 0 (position #\SPACE line))))
               (funcall *callback-function* user-tag)))))

(define-play-beep-tag :text-to-speech (text)
  ;; Plays a string as speech. ARGS contains
  ;; one element--the string to be played.
  (text-to-speech text))

(define-play-beep-tag :full-halt-play-back ()
  (destroy-all-threads-except (current-thread))
  ;; On Linux, when a thread is destroyed, the child
  ;; processes are reparented to another process in
  ;; the same thread group ID, so kill all relevant
  ;; processes again.
  (halt-currently-playing-audio))

(define-play-beep-tag :halt-play-back ()
  (halt-currently-playing-audio))

(define-play-beep-tag :user-tag-name (user-tag)
  ;; Plays a user tag specified by USER-TAG.
  (let* ((user-tag (find-user-tag user-tag))
         (file-name (second user-tag))
         (start (position "-k" user-tag :test #'string=))
         (end (position "-K" user-tag :test #'string=)))
    (when file-name
      (play-file (merge-pathnames *sounds-directory* file-name)
                 :start (when start
                          (nth (1+ start) user-tag))
                 :end (when end
                        (nth (1+ end) user-tag))))))

(define-play-beep-tag :grep-tag-names (&rest args)
  ;; Play all sound files that contain all
  ;; all terms in ARGS.
  (loop for file in (shuffle-sequence (grep-sound-files args))
        do (play-file file)))

(define-play-beep-tag :grep-user-tag-names (&rest args)
  ;; Grab all tag names that contain all terms in ARGS.
  ;; Then play each individual tag.
  (loop for user-tag in (shuffle-sequence (grep-user-tags args))
        do (play-beep-tag :user-tag-name (list user-tag))))

(define-play-beep-tag :tag-name (file-name &optional start end user-tag-name)
    ;; The file name should always be provided. The rest of
    ;; the terms can be nil.
    (let ((sound-file (get-sound-file file-name)))
      (cond
        ;; Do nothing if nothing matches.
        ((equal sound-file nil))
        ;; If the file exists, use the whole beep-tag and
        ;; save it if a user-tag-name is provided.
        ((string= file-name (pathname-name sound-file))
         (play-file (get-sound-file file-name)
                    :start start
                    :end end)
         (when user-tag-name
           (add-user-tag :user-tag-name user-tag-name
                         :file file-name
                         :start start
                         :end end)))
        ;; Otherwise, just play a matching file.
        (t (play-file (get-sound-file file-name))))))

(defun batch-play-beep-tag-thread ()
  "Helper function for BATCH-PLAY-BEEP-TAG that
executes as a thread."
  (with-thread-set ((current-thread))
    (dolist (current-beep-tag *type-and-args-list*)
      ;; TODO: possibly switch this to print a message to the main thread's
      ;;       stdout at some point.
      (ignore-errors
        (play-beep-tag (first current-beep-tag) (rest current-beep-tag))))))

(defun batch-play-beep-tag (callback-function type-and-args-list)
  "Plays a batch of beep tags. CALLBACK-FUNCTION is used to relay information
back to the user sending the tag, such as with a :HELP tag, and takes a single
argument of type STRING. TYPE-AND-ARGS-LIST is a list of lists. Each list is
of the form (BEEP-TAG-TYPE &REST ARGS). ARGS is dependent upon BEEP-TAG-TYPE,
and BEEP-TAG-TYPE is any keyword that is of the type BEEP-TAG-TYPE."
  (bt:make-thread #'batch-play-beep-tag-thread
                  :name "batch-play-beep-tag"
                  ;; Set the default bordeaux threads bindings so that the
                  ;; callback-function and type-and-arts-list is locally set for each thread.
                  :initial-bindings `((*callback-function* . ',callback-function)
                                      (*type-and-args-list* . ',type-and-args-list))))
