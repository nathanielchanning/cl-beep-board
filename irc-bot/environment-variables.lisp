(in-package #:irc-bot)


(define-environment-parameter *nickname* ("BEEP_BOARD_IRC_NICK_NAME" "BeepBoop")
    "The nick name of the IRC bot.")

(define-environment-parameter *irc-server* ("BEEP_BOARD_IRC_SERVER" "10.0.0.14")
    "Address of the IRC server.")

(define-environment-parameter *irc-channel* ("BEEP_BOARD_IRC_CHANNEL" "#general")
    "Channel for the IRC bot.")
