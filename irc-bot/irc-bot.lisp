(in-package #:irc-bot)


(defvar *connection* nil
  "Holds the connection object for the IRC bot.")

(defun handle-beep-tags-in-privmsg (message)
  "Handles all beep tags that occur in MESSAGE.
This function is called on every message that occurs
for the current connection object in *CONNECTION*."
  (flet ((get-output-callback (connection channel)
           (lambda (message)
             (privmsg connection channel message))))
    (let ((channel (first (arguments message)))
          (message (second (arguments message))))
      (batch-play-beep-tag (get-output-callback *connection* channel)
                           (parse-beep-tags message)))))

(defun start-irc-bot-loop ()
  "Starts an irc bot loop that scrapes messages and plays
beep tags based on them."
  (setf *connection* (connect :server *irc-server*
                              :nickname *nickname*))
  (join *connection* *irc-channel*)
  (add-hook *connection* 'irc::irc-privmsg-message 'handle-beep-tags-in-privmsg)
  (read-message-loop *connection*))
