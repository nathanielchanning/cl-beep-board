(in-package #:grammar)


(defun parse-beep-tags (message)
  (flet ((get-all-potential-positions (string)
           "Get the positions of all the beep potential beep tags in STRING.
This assumes an anchor that is #\;."
           (loop with position = -1
                 when (setf position (position #\; string :start (1+ position)))
                   collect position
                 while position)))
    (loop for potential-tag in (get-all-potential-positions message)
          collect (parse 'beep-tag message :start potential-tag :junk-allowed t))))
