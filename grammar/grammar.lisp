(in-package #:grammar)


;;; Each beep-tag starts of with anchor that lets the parser
;;; know that the following characters are potentially a
;;; beep-tag.
(defrule anchor #\;
  (:constant :tag))

;;; A tag name can consist of any alphanumericp character,
;;; '-', and '_'.
(defrule tag-name (+ (or (alphanumericp character) #\- #\_))
  (:text t))

;;; Quotes are used to start and close a text-to-speech tag.
(defrule quote #\"
  (:constant :quote))

;;; Text-to-speech can include at least one of any of the characters
;;; in TAG-NAME, ' ', '.', '!', '?', ''', or ','. TEXT-TO-SPEECH is
;;; be enclosed in QUOTE's and is played by a Text to Speech program.
(defrule text-to-speech (+ (or tag-name #\space #\. #\! #\? #\' #\,))
  (:text t))

(defrule text-to-speech-tag (and quote text-to-speech quote)
  (:destructure (quote1 text-to-speech quote2)
    (declare (ignore quote1 quote2))
    (list :text-to-speech (text text-to-speech))))

;;; Halt playback kills the currently playing sounds and is a single
;;; '!'.
(defrule halt-play-back #\!
  (:constant :halt-play-back))

;;; Full halt playback kills the currently playing sounds and any
;;; sounds that are queued to play in the future. It is two '!'.
(defrule full-halt-play-back "!!"
  (:constant :full-halt-play-back))

;;; User tags are defined by the user and are set-off
;;; by a single '/'.
(defrule user-tag #\/
  (:constant :user-tag))

;;; Help lists all available sound files and is a single '?'.
;;; If it is specified with a preceding USER-TAG, it instead
;;; lists all availabe user-defined tags.
(defrule help #\?
  (:constant :help))

(defrule user-tag-help (and user-tag help)
  (:constant :help-user-tags))

;;; Grep tags can operate on either user tags or
;;; regular beep tags. They are set off by a '*' and
;;; can be used more than once in a single grep beep-tag
;;; to further limit the resulting sounds played. It plays
;;; all sounds that contain all of the grep-terms provided.
(defrule grep-tag #\*
  (:constant :grep-tag))

;;; Separate into a recursive rule so that
;;; a single keyword can be added at the end
;;; of parsing the grep tags.
(defrule grep-tag-names-recursive (and grep-tag tag-name (? grep-tag-names-recursive))
  (:destructure (grep-tag tag-name grep-tag-names)
    (declare (ignore grep-tag))
    (push tag-name grep-tag-names)))

(defrule grep-tag-names (and grep-tag-names-recursive)
  (:lambda (list)
    (push :grep-tag-names (first list))))

(defrule grep-user-tag-names (and user-tag grep-tag-names-recursive)
  (:destructure (user-tag grep-tag-names)
    (declare (ignore user-tag))
    (push :grep-user-tag-names grep-tag-names)))

;;; When creating user tags, the segment of the sound to be played
;;; and its resulting user-tag-name need to be specified. The sound file
;;; is succeeded by an AT-POSITION which contains some combination of
;;; PLAY-BACK-POSITION's and then optionally a BOOKMARK followed by
;;; a TAG-NAME.
(defrule at-position #\@
  (:constant :at-position))

(defrule until #\-
  (:constant :until))

(defrule bookmark #\:
  (:constant :bookmark))

(defrule play-back-position (or (and (+ (digit-char-p character))
                                     (? (and #\. (+ (digit-char-p character)))))
                                (and #\. (+ (digit-char-p character))))
  (:text t))

(defrule play-back-segment (or (and play-back-position until play-back-position)
                               (and play-back-position)
                               (and until play-back-position))
  (:lambda (list)
    (case (length list)
      (1 (list (first list) nil))
      (2 (list nil (second list)))
      (3 (list (first list) (third list))))))

(defrule play-back+bookmark-segment (and at-position
                                         play-back-segment
                                         (? (and bookmark tag-name)))
  (:destructure (at-position play-back-args tag-name)
    (declare (ignore at-position))
    (concatenate 'list play-back-args (list (when tag-name
                                              (second tag-name))))))

(defrule user-tag-name (and user-tag tag-name)
  (:destructure (user-tag tag-name)
    (declare (ignore user-tag))
    (list :user-tag-name tag-name)))

(defrule full-tag (and tag-name (? play-back+bookmark-segment))
  (:destructure (tag-name bookmark-segment)
    (concatenate 'list (list :tag-name tag-name)
                 (if bookmark-segment
                     bookmark-segment
                     (list nil nil nil)))))

;;; The following is the culmination of the previous
;;; rules into what constitutes a beep-tag.
(defrule beep-tag (and anchor
                       (or full-halt-play-back
                           halt-play-back
                           help
                           text-to-speech-tag
                           user-tag-help
                           user-tag-name
                           grep-tag-names
                           grep-user-tag-names
                           full-tag))
  (:destructure (anchor tag)
    (declare (ignore anchor))
    (if (listp tag)
        tag
        (list tag))))
