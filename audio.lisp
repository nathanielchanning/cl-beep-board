(in-package #:audio)

;;; Only supporting ogg vorbis right now.

(defun play-file (file-path &key (start nil) (end nil))
  "Plays the file located at FILE-PATH. Optionally specify
the start and end of the file with the respective keys
:START and :END."
  (declare (type pathname file-path))
  (let ((arg-list (remove-if #'null (list (truename file-path)
                                          (when start
                                            (format nil "-k ~d" start))
                                          (when end
                                            (format nil "-K ~d" end))))))
    (run "ogg123" arg-list)))

(defun halt-currently-playing-audio ()
  "Kills all ogg123 and espeak processes being run."
  (run "killall" '("ogg123" "espeak")))

(defun text-to-speech (text)
  "Translates TEXT into speech via espeak."
  (declare (type string text))
  (run "espeak" (list text)))
