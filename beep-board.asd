(in-package #:cl-user)

(ql:quickload '(usocket esrap external-program
                split-sequence alexandria bordeaux-threads
                cl-irc))

(defpackage beep-board-asd
  (:use cl asdf))

(in-package #:beep-board-asd)

(defsystem beep-board
  :depends-on ("usocket" "esrap" "external-program"
               "bordeaux-threads" "uiop" "split-sequence"
               "alexandria")
  ;; Could probably just flatten this into a
  ;; single directory since each module is just amounting
  ;; to one file anyways, though I doubt it really matters
  ;; much anyways.
  :components
  ((:file "package")
   (:file "environment-variables")
   (:file "audio")
   (:module "beep-tag"
            :components
            ((:file "thread-set")
             (:file "environment-variables")
             (:file "util")
             (:file "beep-tag")))
   (:module "grammar"
            :components
            ((:file "grammar")
             (:file "beep-tag")))
   (:module "irc-bot"
            :components
            ((:file "environment-variables")
             (:file "irc-bot")))))

(load-system 'beep-board)
